## Hillel course Python advanced - Home work 5

### Clone my home work project from Gitlab:

    https://gitlab.com/marchenko.alex/hillel_home_work_5.git

### souchange working directory to **hillel_home_work_5**

    cd ~/Your_path_to_the_folder/hillel_home_work_5

> also you can open terminal and drag required folder inside Terminal window


### Now you should install **pip** and **virtualenv**
### Install **pip** first
 
    sudo apt install python-pip

### Then install **virtualenv** using pip3

    pip3 install virtualenv --user

### Now create a virtual environment 

    virtualenv venv 

>I'll use name **venv**, but you can use any name instead of it

  
### Activate your virtual environment:    
    
    source venv/bin/activate

### Install necessary python libraries 

    pip install -r requirements.txt
    
### In order to deactivate virtual environment:

    deactivate

### Export required python environment variables:

    export FLASK_APP=app.py
    export FLASK_ENV=development
    
### You can check if environment variables were set up correctly:   
    
    env | grep FLASK
    
### if you have following, it means that all is OK

    FLASK_APP=app.py
    FLASK_ENV=development

    
### Now you can run Flask:

    flask run


