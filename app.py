from markupsafe import escape
from flask import Flask
from faker import Faker
from faker.providers import internet
import pandas as pd
import os
import requests
import base58


app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello, World!'


@app.route('/requirements/')
def requirements():
    with open("requirements.txt", "r") as f:
        content = f.read()
    return f'<pre>{content}</pre>'


@app.route('/generate-users/<int:qty_users>')
def generate_users(qty_users):
    fake = Faker('ru_RU')
    fake.add_provider(internet)
    list_users_data = ''
    for i in range(qty_users):
        name = fake.name()
        email = fake.email()
        list_users_data += f"{i + 1}. {name} - {email}<br>"
    return f'<pre>{list_users_data}</pre>'


@app.route('/mean/')
def parse_csv():
    df = pd.read_csv('hw05.csv')
    parsed_file = os.path.abspath('hw05.csv')
    total_values_in_file = df['Index'].count()
    average_height = round((df[' "Height(Inches)"'].median() * 2.54), 1)
    average_weight = round((df[' "Weight(Pounds)"'].median() * 0.45359237), 1)

    return f'Parsed file: {parsed_file}<br>' \
           f'Total values in file (strings of data): {total_values_in_file}<br>' \
           f'Average height: {average_height} cm<br>' \
           f'Average weight: {average_weight} kg<br>'


@app.route('/space/')
def astronauts():
    r = requests.get(url='http://api.open-notify.org/astros.json')
    astros = r.json()['number']
    return f'Presently {astros} astronauts in the space'


@app.route('/base58encode/<string:string>')
def coder(string):
    encoded_string = base58.b58encode(string)
    return f'{escape(encoded_string)}'


@app.route('/base58decode/<string:string>')
def decoder(string):
    decoded_string = base58.b58decode(string)
    return f'{escape(decoded_string)}'
